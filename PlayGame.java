package monopoly;

public class PlayGame {
    
    /**
     * Throw the dice, and move the designated amount.
     * If you threw doubles and they're not 1's, move another dice.
     * If you threw doubles and they're 1's, natural one: move only one spot.
     * @param x Player
     * @return String move : whatever the player moves - output

     */
    public static String movePlayer(Player x) {
        int a = Dice.roll();
        int b = Dice.roll();
        int c = 0;
        String move;
        
        if(a==b) {
            if(a==1) {
                move = "Natural one... Moving only 1 spot...";
                b=0;
            } else {
                move = "You threw doubles (" + a + " & " + b + ")! You get to throw another dice!";
                c = Dice.roll();
                move += "\nYou rolled " + c + "! Moving another " + c + " spots!";
            }
        } else {
            move = "You rolled " + a + " and " + b + "! Nicely done!";
        }
        move +="\nMoved from " + x.getState();
        x.setState( x.getState() + a + b + c);
        move += " to " + x.getState() + ". \n";
        
        return move;
    }
    
    /**
     *
     * @param numPlayers : int
     * @param players : Player[]
     * @param streets : Street[]
     * @param fonds : Fonds
     * @param kans : Kans
     * @param numTurns : int
     * @param counter : int
     * @param activePlayer : int
     */
    public static void Playing(int numPlayers, Player[] players, Street[] streets, Fonds fonds, Kans kans, int numTurns, int counter, int activePlayer) {
        boolean finished = false, turnFinished;
        String move, street, buyStreet;
        
        while( !finished) {
            numTurns++;
            for(int i=0; i<numPlayers; i++) {
                turnFinished = false;
                counter = 0;
                move = movePlayer( players[i]);
                street = Menu.actionState(streets, players, i, players[i].getState(), numPlayers, fonds, kans);
                Menu.payRivalPlayer(streets, players, i, players[i].getState());
            
                while ( !turnFinished ) { // keep going untill finish turn is selected
                    if( counter != -4) { // Wants to quit
                        Systeem.ClearScreen();
                    } 
                    buyStreet = Menu.buyStreet(streets, players, i, players[i].getState(), numPlayers, fonds, kans);
                    Menu.printStatus(numPlayers, players, numTurns, i, move);
                    Menu.printAction(counter, street, buyStreet);
                    counter = Menu.askAction(counter);
                    
                    System.out.println("Counter : " + counter);
                    if(counter < 0) { 
                        switch(counter) {
                            case -1:
                                if( "Buy".equals( buyStreet.substring(0,3) ) ){
                                    players[i].setMoney( players[i].getMoney() - streets[ players[i].getState() ].getCost() );
                                    streets[ players[i].getState() ].setOwner(i);
                                } else if (("Already bought by".equals( buyStreet.substring(0,17) ) ) 
                                        && (! "yourself".equals( buyStreet.substring(18,26) )) ) {
                                    int rent = streets[ players[i].getState()].getRent();
                                    players[i].setMoney( players[i].getMoney() - rent);
                                    players[ streets[ players[i].getState() ].getOwner() ].setMoney( players[ streets[ players[i].getState() ].getOwner() ].getMoney() + rent);
                                }
                                break;
                            case -2:
                                turnFinished = true;
                                break;
                            case -3: 
                                SaveGame.saveGame(players, streets, numPlayers, numTurns, i, counter);
                                break;
                            case -4:
                                System.out.print("Are you sure? There's no going back! (Y/N) : ");
                                String answer = Systeem.readString();
                                System.out.println("");
                                if( null != answer.substring(0,1) ) switch (answer.substring(0,1)) {
                                    case "Y":
                                    case "y":
                                        Systeem.ClearScreen();
                                        System.exit(0);
                                    case "N":
                                    case "n":
                                    default :
                                        Systeem.ClearScreen();
                                        break;
                                }
                                break;
                            default: break;
                        }
                    }
                }    
                    
            } // inner loop: lets every player take a turn
        } 
        
    }
    
}
