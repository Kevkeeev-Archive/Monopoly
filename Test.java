package monopoly;

public class Test {
    
    /**
     * testEnvironment: test environment
     * returns: void
     */
    public static void testEnvironment() {
        // Function to test whatever other function should do.
        
     
        
        
    } // end of testEnvironment
    
    /**
     * TestFillPlayers: admin function to quickly enter 6 players midgame.
     * @return Player[] players
     */
    public static Player[] TestFillPlayers() {
        
        Player[] players = new Player[6];
        
        players[0] = new Player("Test", "Red");
        players[1] = new Player("Tosti", "Blue");
        players[2] = new Player("Tuck", "Green");
        players[3] = new Player("Toink", "Yellow");
        players[4] = new Player("Tierelier", "Purple");
        players[5] = new Player("Tarzan", "Brown");
        
        players[0].setMoney(15600);
        players[1].setMoney(79200);
        players[2].setMoney(600);
        players[3].setMoney(74961500); // $ 74.961.500
        players[4].setMoney(25800);
        players[5].setMoney(100);
        
        players[0].setState(11);
        players[1].setState(5);
        players[2].setState(28);
        players[3].setState(17);
        players[4].setState(33);
        players[5].setState(39);
        
        return players;
    }
}
