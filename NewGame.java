package monopoly;

public class NewGame {
    
    /**
     * Menu for starting a new game.
     */
    public static void NewGameMenu() {
        
        int numPlayers = Setup.askPlayers();
        Player[] players = Setup.fillPlayers(numPlayers);
        Street[] streets = Street.fillStreets();
        
        Fonds fonds = new Fonds();
        Kans kans  = new Kans();
        
        PlayGame.Playing(numPlayers, players, streets, fonds, kans, 0, 0, 0);

    }
    
}
