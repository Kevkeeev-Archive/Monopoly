package monopoly;

public class LoadGame {
    
    
    
    public static void LoadSave(){
        int i, j;
        int numPlayers = Systeem.readInt();
        int numTurns = Systeem.readInt();
        int activePlayer = Systeem.readInt();
        int counter = Systeem.readInt();
        Systeem.readString();
        Fonds fonds = new Fonds();
        Kans kans = new Kans();
        
        Player[] players = new Player[numPlayers];
        Street[] streets = new Street[40];
        
        for(i=0; i<numPlayers; i++){
            players[i] = new Player( Systeem.readString(), Systeem.readString() );
            players[i].setMoney( Systeem.readInt() ); 
            players[i].setOutOfJailCards( Systeem.readInt() );
            players[i].setType( Systeem.readInt() );
            players[i].setInitiative( Systeem.readInt() );
            players[i].setState( Systeem.readInt() );
            Systeem.readString();
        }
        
        String a, b;
        int c, d, e;
        int[] f = new int[6];
        
        for(i=0; i<40; i++) {
            a = Systeem.readString(); // Name
            b = Systeem.readString(); // Description
            c = Systeem.readInt();    // city
            d = Systeem.readInt();    // cost
            e = Systeem.readInt();    // houseCost
            Systeem.readString();
            for(j=0; j<6; j++) {      // rent[]
                f[j] = Systeem.readInt();
            }
            Systeem.readString();
            
            streets[i] = new Street(a, b, c, d, e, f);
            streets[i].setNumHouses( Systeem.readInt() );
            streets[i].setActive( Systeem.readInt() );
            streets[i].setOwner( Systeem.readInt() );
            Systeem.readString();
        }
        
        PlayGame.Playing(numPlayers, players, streets, fonds, kans, numTurns, counter, activePlayer);
        
    }
            
            
    /**
     * Loading a saved game.
     * For now: it loads an admin set of players midgame using a designated Test function.
     */     
    public static void LoadGame() {
//        System.out.println("LOADING/SAVING NOT YET IMPLEMENTED! ");

        int numPlayers = Setup.askPlayers();
        int turns = 13;
        Player[] players = Test.TestFillPlayers();
        Street[] streets = Street.fillStreets();
        Fonds fonds = new Fonds();
        Kans kans = new Kans();
        PlayGame.Playing(numPlayers, players, streets, fonds, kans, turns, 0, 0);
    
    
    }
}
