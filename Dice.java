package monopoly;
import java.util.Random;

public class Dice {
    
    /**
     * Rolls a d6 using a random generator.
     * @return int dice
     */
    public static int roll() {
        Random randomGen = new Random();
        return randomGen.nextInt(6) + 1;
    }
    
}
