package monopoly;
import java.util.Scanner;

public class MainMenu { 
    
    /**
     * Main menu of Monopoly
     */
    public static void Menu(){
        int counter = 0;
        boolean keepRunning = true;
        String keyboard; // want to use keyPressed() but is a lot of work, this is, for now, easier to use.
        Scanner textscan = new Scanner(System.in);
        
        while( keepRunning ) {
            Systeem.ClearScreen();
            System.out.println( (counter==0? " > " : "   ") + " Introduction ");
            System.out.println( (counter==1? " > " : "   ") + " Rules ");
            System.out.println( (counter==2? " > " : "   ") + " Start New Game ");
            System.out.println( (counter==3? " > " : "   ") + " Load Previous Game");
            System.out.println( (counter==4? " > " : "   ") + " Exit Monopoly");
            System.out.print("\n    Press W/S to move cursor, Enter to select, or type 'kill' to exit. ");
            
            keyboard = textscan.nextLine();
            System.out.println("");
            
            switch(keyboard) {  
                case "W" :
                case "w" :
                    if(counter==0) {
                        counter = 4;
                    } else {
                        counter--;
                    }
                    break;
                case "S" :
                case "s" :
                    counter = (counter + 1) % 5;
                    break;
                case "kill" :
                    System.out.println("Bye Bye!");
                    System.exit(0);
                    break;
                case "admin" :
                    LoadGame.LoadGame();
                    break;
                case "" :
                    switch(counter) {
                        case 0 : 
                            Intro.printIntro();
                            break;
                        case 1 :
                            Rules.printRules();
                            break;
                        case 2 :
                            NewGame.NewGameMenu();
                            break;
                        case 3 :
                            LoadGame.LoadGame();
                            break;
                        case 4 :
                            keepRunning = false;
                            System.out.println("Bye Bye!");
                            break;
                        default: break;
                    }
                    break;
                default: break;
            }
        }
    }
}